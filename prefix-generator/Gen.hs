module Gen where

import Mult (Mu(..), Pat, Clause(..), sameLengths)
import Test
import Data.List (permutations)
import Control.Monad
import qualified Data.Set as Set

import Debug.Trace

emptyMu = Mu []

isSingleton :: [a] -> Bool
isSingleton [_] = True
isSingleton  _  = False

-- patternN generates all patterns of a given total length

patternN :: Int -> [Pat]
patternN n = map (map length) $
  filter (\xs -> not (isSingleton xs || sameLengths xs))
  (splits [1..n])

-- Given m :: Mu of length (n-1), the value extendBy n m is a list of valid
-- prefixes of length n that agree with m. The key observation is that
-- only a few counterexamples are enough to get rid of huge numbers of cases,
-- so we keep successful counterexamples in a set ant try them first when
-- checking subsequent cases.

extendBy :: Int -> Mu -> [Mu]
extendBy n m = aux m (patternN n) allPerms Set.empty []
 where
  allPerms = permutations [0 .. (n-1)]
  -- 
  aux :: Mu -> [Pat] -> [[Int]] -> Set.Set [[[Int]]] -> [Mu] -> [Mu]
  aux m [] _ ces accum = m : accum
  aux oldMu@(Mu i) allPats@(pat : pats) (perm : perms) ces accum =
    let m = Mu (Clause pat perm : i)
    in case testNCE ces n m of
         SeemsFine -> aux m pats allPerms ces (aux oldMu allPats perms ces accum)
         Counterexample c -> aux oldMu allPats perms (Set.insert c ces) accum
  aux m (_:_) _ _ accum = accum

searchTree :: [Mu]
searchTree = do
  m3 <- extendBy 3 emptyMu
  m4 <- extendBy 4 m3
  m5 <- extendBy 5 m4
  m6 <- extendBy 6 m5
  return m6
