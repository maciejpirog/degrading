module Main where

import System.IO

import Mult (Mu)        -- Mu is a type of a definition of a prefix of length 6
import Gen (searchTree) -- searchTree is a lazy list of valid prefixes

import qualified Data.Set as Set

main :: IO ()
main = do
  putStrLn "\nLet's go!"
  hFlush stdout
  processSearchTree 0 searchTree

processSearchTree :: Int -> [Mu] -> IO ()
processSearchTree i [] = do
  putStrLn $ "\nDONE! Found " ++ show i ++ " possible prefixes"
processSearchTree i (m:ms) = do
  putStrLn $ show m
  hFlush stdout
  processSearchTree (i+1) ms
