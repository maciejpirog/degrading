{-# LANGUAGE Strict #-}

module Test where

import Mult

import qualified Data.Set as Set

data Result = Counterexample [[[Int]]] | SeemsFine

splits :: [Int] -> [[[Int]]]
splits [x] = [[[x]]]
splits (x:xs) = map ([x]:) (splits xs) ++ map (\ys -> (x : head ys) : tail ys) (splits xs)

doubleSplits :: [Int] -> [[[[Int]]]]
doubleSplits xs = splits xs >>= mapM splits

testNCE :: Set.Set [[[Int]]] -> Int -> Mu -> Result
testNCE ces n m =
  case filter (not . check)
              (Set.toList ces ++
              (case n of 4 -> tests4
                         5 -> tests5
                         6 -> tests6
                         _ -> do { b <- [3..(n-1)]; doubleSplits [0..b] } ))
    of []    -> SeemsFine
       (c:_) -> Counterexample c
 where
  join :: Show a => [[a]] -> [a]
  join = mu m
  check :: [[[Int]]] -> Bool
  check xsss = let l = join (join xsss)
                   r = join (fmap join xsss)
               in null l || null r || l == r

tests4 = doubleSplits [0..3]
tests5 = tests4 ++ doubleSplits [0..4]
tests6 = tests5 ++ doubleSplits [0..5]
