{-
This module gives a description of a prefix of a multiplication

* Mu is a definition of a prefix
* mu is a partial multiplication of the non-empty list monad

We use usual lists to represent non-empty lists, making sure we don't
treat empty list as a valid value.
-}

{-# LANGUAGE Strict #-}

module Mult where

-- A prefix Mu is represented as a list of clauses,
-- each clause contains a pattern and a value.

-- A pattern is a length of given sublists,
-- E.g., [2,3,1] represents the pattern [[a,b],[c,d,e],[f]]

type Pat = [Int]

-- Since we know that each RHS of a pattern is a permutation of
-- variables in the LHS, we represent the RHS as a list of values
-- representing the index of a varaible in the (flattened) pattern
-- E.g., the clause
-- mu [[a,b],[c,d,e],[f]] = [a,e,f,b,d,c]
-- is represented as
-- Clause [2,3,1] [0,4,5,1,3,2]

data Clause = Clause { pat :: Pat,  permutation :: [Int] }

-- A prefix is then just a list of clauses

newtype Mu = Mu [Clause]

-- Turning a Mu into a partial join for lists. This is to test
-- if a given prefix is a viable candidate.

sameLength :: [a] -> [a] -> Bool
sameLength [] [] = True
sameLength _  [] = False
sameLength [] _  = False
sameLength (x:xs) (y:ys) = sameLength xs ys

sameLengths :: [[a]] -> Bool
sameLengths (xs : yss@(ys :_)) = sameLength xs ys && sameLengths yss
sameLengths [_] = True
sameLengths [ ] = False

-- applyPerm i xs permutates xs according to the recipe in i
-- E.g., applyPerm [2,1,0,3] "abcd" = "cbad"

applyPerm :: [Int] -> [a] -> [a]
applyPerm info xs = map (xs !!) info

-- patternFits checks if a pattern applies to a value

patternFits :: Pat -> [[a]] -> Bool
patternFits [] []                            = True
patternFits (n:ns) (xs:xss) | n == length xs = patternFits ns xss
                            | otherwise      = False
patternFits _ _                              = False

mu :: (Show a) => Mu -> [[a]] -> [a]
mu (Mu (Clause pat i : cs)) xss | patternFits pat xss = applyPerm i (concat xss)
                                | otherwise           = mu (Mu cs) xss
mu _ xss = [] -- empty list means "undefined value for mu"

-- Pretty printing a definition of Mu

showClause :: Clause -> String
showClause (Clause pat perm) = "mu "
  ++ "[" ++ showPat pat ['a'..] ++ "] = "
  ++ showPerm perm ['a'..] ++ "\n"
 where
  showPat [] _ = ""
  showPat [n] az = showExList (take n az)
  showPat (n : ns) az = showExList (take n az) ++ "," ++ showPat ns (drop n az) 
  showPerm p az = showExList $ map (az !!) p
    
showExList :: String -> String
showExList xs = "[" ++ aux xs ++ "]"
 where
  aux [] = ""
  aux [c] = [c]
  aux (c:cs@(_:_)) = c : ',' : aux cs

instance Show Mu where
  show (Mu i) = "\n" ++ (concat $ map showClause i)

